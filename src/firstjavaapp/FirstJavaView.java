/*
 * FirstJavaView.java
 */

package firstjavaapp;

import java.awt.event.ActionEvent;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import org.jdesktop.application.Action;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.application.FrameView;

/**
 * The application's main frame.
 */
public class FirstJavaView extends FrameView implements ActionListener {

    public FirstJavaView(SingleFrameApplication app) {
        super(app);

        initComponents();
        
        JFrame mainFrame = this.getFrame();
        mainFrame.setLayout(new BorderLayout());
        mainFrame.setSize(105, 35);
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        JMenu about = new JMenu("About");
        JMenuItem license = new JMenuItem("License");
        license.addActionListener(this);
        about.add(license);
        menuBar = new JMenuBar();
        menuBar.add(new JMenu("MenuWidget1"));
        menuBar.add(about);
        
        mainPanel = new JPanel();
        mainPanel.setLayout(new GridLayout(2, 2));
        mainPanel.setBorder(new CompoundBorder(BorderFactory.createTitledBorder("Panel Caption"),
                new EmptyBorder(30, 10, 10, 10)));
        
        JToolBar toolbar = new JToolBar();
        toolbar.add(new JButton("ToolbarButton"));
        toolbar.add(new JCheckBox("ToolbarCheckbox", true));
        
        JPanel radioPanel = new JPanel();
        radioPanel.setLayout(new BoxLayout(radioPanel, BoxLayout.Y_AXIS));
        JRadioButton radio1 = new JRadioButton("Radio Button 1");
        JRadioButton radio2 = new JRadioButton("Radio Button 2");
        JRadioButton radio3 = new JRadioButton("Radio Button 3");
        JRadioButton inactiveRadio = new JRadioButton("Inactive Radio");
        inactiveRadio.setEnabled(false);
        
        radioPanel.add(radio1);
        radioPanel.add(radio2);
        radioPanel.add(radio3);
        radioPanel.add(inactiveRadio);
        
        JPanel panel12 = new JPanel();
        panel12.setLayout(new BorderLayout());
        panel12.add(radioPanel, BorderLayout.CENTER);
        panel12.add(new JButton("Button"), BorderLayout.SOUTH);
        
        ButtonGroup radioGroup = new ButtonGroup();
        radioGroup.add(radio1);
        radioGroup.add(radio2);
        radioGroup.add(radio3);
        radioGroup.add(inactiveRadio);
        
        panel1.setBorder(new CompoundBorder(BorderFactory.createTitledBorder("Panel"),
                new EmptyBorder(10, 10, 10, 10)));
        panel1.setLayout(new BorderLayout());
        panel1.add(new List(), BorderLayout.WEST);
        panel1.add(panel12, BorderLayout.CENTER);
        
        JTextField textField = new JTextField("Text Field");
        JPasswordField passwordField = new JPasswordField("Text Field");
        JComboBox comboBox = new JComboBox();
        textField.setMaximumSize(new Dimension(Integer.MAX_VALUE, 30));
        passwordField.setMaximumSize(new Dimension(Integer.MAX_VALUE, 30));
        comboBox.setMaximumSize(new Dimension(Integer.MAX_VALUE, 30));
        
        JCheckBox uncheckedBox = new JCheckBox("Unchecked CheckBox");
        JCheckBox checkedBox = new JCheckBox("Checked CheckBox");
        JCheckBox inactiveCheckedBox = new JCheckBox("Inactive CheckBox");
        checkedBox.setSelected(true);
        inactiveCheckedBox.setEnabled(false);
        JPanel checkBoxPanel = new JPanel();
        checkBoxPanel.setLayout(new BoxLayout(checkBoxPanel, BoxLayout.Y_AXIS));
        checkBoxPanel.add(uncheckedBox);
        checkBoxPanel.add(checkedBox);
        checkBoxPanel.add(inactiveCheckedBox);
        
        JPanel tabPanel = new JPanel();
        tabPanel.setLayout(new BorderLayout());
        tabPanel.add(checkBoxPanel, BorderLayout.CENTER);
        JSlider slider = new JSlider(JSlider.HORIZONTAL,
                                              0, 100, 50);
        tabPanel.add(slider, BorderLayout.SOUTH);
        
        JTabbedPane tabs = new JTabbedPane();
        tabs.addTab("Selected Tab", new JLabel());
        tabs.addTab("Other Tab", new JLabel());
        tabs.setComponentAt(0, tabPanel);
        panel2.setLayout(new BoxLayout(panel2, BoxLayout.Y_AXIS));
        panel2.setBorder(new EmptyBorder(10, 10, 10, 10));
        panel2.add(tabs);
        
        panel3.setLayout(new BoxLayout(panel3, BoxLayout.Y_AXIS));
        panel3.setBorder(new EmptyBorder(10, 0, 10, 0));
        panel3.add(textField);
        panel3.add(passwordField);
        panel3.add(comboBox);
        
        JTextArea textArea = new JTextArea("Text Area");
        textArea.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
        panel4.setLayout(new BoxLayout(panel4, BoxLayout.Y_AXIS));
        panel4.setBorder(new EmptyBorder(10, 10, 10, 10));
        panel4.add(textArea);
        
        mainPanel.add(panel1);
        mainPanel.add(panel2);
        mainPanel.add(panel3);
        mainPanel.add(panel4);
        
        mainFrame.add(toolbar, BorderLayout.NORTH);
        mainFrame.add(mainPanel, BorderLayout.CENTER);
        mainFrame.setJMenuBar(menuBar);
    }

    @Action
    public void showAboutBox() {
        if (aboutBox == null) {
            JFrame mainFrame = FirstJavaApp.getApplication().getMainFrame();
            aboutBox = new FirstJavaAboutBox(mainFrame);
            aboutBox.setLocationRelativeTo(mainFrame);
        }
        FirstJavaApp.getApplication().show(aboutBox);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
    private void initComponents() {
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel mainPanel;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JProgressBar progressBar;
    private javax.swing.JLabel statusAnimationLabel;
    private javax.swing.JLabel statusMessageLabel;
    private javax.swing.JPanel statusPanel;
    // End of variables declaration//GEN-END:variables

    private JDialog aboutBox;
    private JPanel panel1 = new JPanel();
    private JPanel panel2 = new JPanel();
    private JPanel panel3 = new JPanel();
    private JPanel panel4 = new JPanel();

    public void actionPerformed(ActionEvent ae) {
        showAboutBox();
    }
}
